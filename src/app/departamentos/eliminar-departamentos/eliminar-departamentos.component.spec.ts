import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarDepartamentosComponent } from './eliminar-departamentos.component';

describe('EliminarDepartamentosComponent', () => {
  let component: EliminarDepartamentosComponent;
  let fixture: ComponentFixture<EliminarDepartamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarDepartamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarDepartamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
