import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarDepartamentosComponent } from './departamentos/listar-departamentos/listar-departamentos.component';
import { CrearDepartamentosComponent } from './departamentos/crear-departamentos/crear-departamentos.component';
import { ActualizarDepartamentosComponent } from './departamentos/actualizar-departamentos/actualizar-departamentos.component';
import { EliminarDepartamentosComponent } from './departamentos/eliminar-departamentos/eliminar-departamentos.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarDepartamentosComponent,
    CrearDepartamentosComponent,
    ActualizarDepartamentosComponent,
    EliminarDepartamentosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
